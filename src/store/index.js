import Vue from "vue";
import Vuex from "vuex";

import { module } from "./modules";
import { subModule } from "../submodule/store/modules";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        module,
        subModule
    },
});
