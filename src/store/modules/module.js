export const module = {
    namespaced: true,

    strict: process.env.NODE_ENV !== "production",

    state: {
        text: 'Module message!'
    },
}